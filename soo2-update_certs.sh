#!/bin/bash
# Script Licence: GNU/GPL v3
#   2017-2018 - Eric Seigne <eric.seigne@abul.org>
# automatic renew let's encrypt certificates

LINKTOTEST="/var/lib/dehydrated/certs/mail.sud-ouest2.org/privkey.pem"
TESTCERTBEFORE=`readlink -f ${LINKTOTEST}`

dehydrated -c

TESTCERTAFTER=`readlink -f ${LINKTOTEST}`

if [ "${TESTCERTBEFORE}" != "${TESTCERTAFTER}" ]; then
    #relancer les services qui font reference a ces certificats ...
    systemctl restart postfix
    systemctl restart dovecot
fi

#ensuite il faut propager les certificats sur les serveurs proxy frontaux ...
chown root:ssl-cert /var/lib/dehydrated/certs/ -R
find /var/lib/dehydrated/certs/ -type f -exec chmod 640 {} \;
find /var/lib/dehydrated/certs/ -type d -exec chmod 750 {} \;
