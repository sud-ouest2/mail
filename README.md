# Outils spécifique pour le serveur mail principal

## Configuration postfix

voir l'installation de Modoboa

## Certificats Let's encrypt

voir le script soo2-update_certs.sh et le cron associé

## Règles de routage spéciales pour les listes de diffusions

voir le script soo2-update_transport_listes.sh et le cron associé

