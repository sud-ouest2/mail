#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
#pour router directement les mails a destination des mailing listes

#serveur sympa dans notre infra
SYMPASRV="sympa-01.sud-ouest2.org"

FICLISTES=`mktemp`

LADATE=`date "+%Y%m%d %H:%M"`
echo "------------- ${LADATE}"

wget -q https://sud-ouest2.org/dolibarr/public/soo/get_liste_virtualhosts.php -O ${FICLISTES}
if [ $? != 0 ]; then
    #erreur de transfert ou autre
    exit
fi

diff ${FICLISTES} /etc/postfix/referencelistes
if [ $? == 0 ]; then
    rm ${FICLISTES}
    exit    
fi

cp /etc/postfix/transport_regexp_listes.orig /etc/postfix/transport_regexp_listes

for domaine in `cat ${FICLISTES}`
do
    echo "  liste $domaine"
    #/.*@listes\..*/ smtp:${SYMPASRV}
    echo "/.*@${domaine}/ smtp:${SYMPASRV}" >> /etc/postfix/transport_regexp_listes
done

cp ${FICLISTES} /etc/postfix/referencelistes
rm ${FICLISTES}

service postfix restart
